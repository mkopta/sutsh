CFLAGS   += -Os
CFLAGS   += -ansi
CFLAGS   += -Wall -Wextra -Wconversion -pedantic
#CFLAGS   += -DDEBUG -ggdb3
CLIBS     =
sfx       = c
name      = sutsh
args      =

all: build

build: $(name)

$(name): $(name).$(sfx)
	$(CC) $(CFLAGS) $(CLIBS) -o $(name) $(name).$(sfx)

run:
	./$(name) $(args)

clean:
	rm -f $(name)
