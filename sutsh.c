/*
sutsh - minimal shell for learning purposes
Copyright (C) 2009 Martin Kopta <martin@kopta.eu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#define MAX_ARGS 32
#define MAX_ARG_LEN 512
#define BUFLEN ((MAX_ARGS * MAX_ARG_LEN) + 1)

static char *mystrdup(const char* str) {
  char *copy = malloc(strlen(str) * sizeof(char) + 1);
  if (copy != NULL) strcpy(copy, str);
  return copy;
}

int main(void) {
  int status = -1, i = 0, paramc = 0;
  char input[BUFLEN], *input_copy, *token, *newline = input;
  char command[MAX_ARG_LEN], **parameters = malloc(MAX_ARGS * sizeof(char *));
LOOP: fputs("sutsh% ", stdout);
  fflush(stdout);
  memset(parameters, '\0', MAX_ARGS * sizeof(char *));
  memset(input, '\0', BUFLEN);
  paramc = 0;
  status = -1;
  if (fgets(input, BUFLEN, stdin)) {
    newline = strchr(input, '\n');
    input[newline - input] = '\0';
    input_copy = malloc((strlen(input) + 1) * sizeof(char));
    input_copy = strcpy(input_copy, input);
    token = strtok(input_copy, " ");
    if ((token != NULL) && (strlen(token) < (MAX_ARG_LEN - 1))) {
      strcpy(command, token);
      parameters[0] = mystrdup(token);
      token = strtok(input, " ");
      for (i = 1; (i < MAX_ARGS) && (parameters[i - 1] != NULL); ++i) {
        token = strtok(NULL, " ");
        if ((token != NULL) && (strlen(token) < (MAX_ARG_LEN - 1))) {
          parameters[i] = mystrdup(token);
          paramc = paramc + 1;
        }
      }
      status = 0;
    }
    free(input_copy);
  }
  if (status != -1) {
    if (strcmp(command, "exit") == 0) goto END;
    if (strcmp(command, "cd") == 0) {
      if(chdir(parameters[1]))
        perror("sutsh: cd");
      for (i = 0; i < paramc; ++i)
        free(parameters[i]);
      goto LOOP;
    }
    if (fork() != 0) {
      for (i = 0; i < paramc; ++i)
        free(parameters[i]);
      waitpid(-1, &status, 0);
    } else {
      execvp(command, parameters);
      perror("sutsh: exec");
      return 1;
    }
  }
  goto LOOP;
END: return 0;
}
